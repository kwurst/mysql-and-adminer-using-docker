# MySQL using Docker

## Install Docker Desktop

- Mac: https://docs.docker.com/docker-for-mac/install/
- Windows 10 Pro, Enterprise, and Education: https://docs.docker.com/docker-for-windows/install/
- Windows 10 Home: https://docs.docker.com/docker-for-windows/install-windows-home/

## Running MySQL

1. Make sure `Docker Desktop` is running on your computer.
2. Start up `MySQL`: From your terminal type `docker-compose up --detach` or double-click `mysql-up.bat`
    - This will take a while the first time as it downloads the software needed. It will be much faster after that.
3. Connect to `Adminer` by opening http://localhost:8080 in your browser
4. Username: `root`
5. Password: `example`

## Stopping MySQL

1. Log out from `Adminer` (button in upper-right corner.)
2. Shut down `MySQL`: From your terminal type `docker-compose down` or double-click `mysql-down.bat`
3. Quit `Docker Desktop`.
